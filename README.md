# CricStore


## Team

- Chetas Borse      111803045
- Rohit Chaudhari   111803046



## Project Statement

CricStore is an application to store, retrieve and display the cricket stats. In this application, user can view and analyze statistics of a particular player or team from any league.



## Steps To Run:

1. Clone the project.

2. In the backend as well as parent directory run ```npm install```.

3. Create Database **cricket** with the tables as mentioned in ```backend/script.sql```. For data you can use ```backend/data```.

4. Paste your mysql configuration in the ```backend/config``` file.

5. Run command ```npm run app``` to run the application.
